package io.gitlab.grumpybear4257.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class CommonUtils {
    /**(
     * Converts a list of strings to a single string of all items, with each item in the list being separated by the specified characters
     * @param input The list of strings to combine together
     * @param separator String of characters that are used to separate each value
     * @return The combined string
     */
    public static String GetString(Iterable<String> input, String separator) {
        StringBuilder output = new StringBuilder();
        for (String value : input) {
            output.append(value);
            output.append(separator);
        }

        return TrimEnd(output.toString(), separator.toCharArray());
    }

    /**(
     * Converts a list of strings to a single string of all items, with each item in the list being separated by a space
     * @param input The list of strings to combine together
     * @return The combined string
     */
    public static String GetString(Iterable<String> input) {
        return GetString(input, " ");
    }

    /**
     * Converts an array of strings to a single string of all items, with each item in the list being separated by the specified characters
     * @param input The array of string to combine together
     * @param separator String of characters that are used to separate each value
     * @return The combined string
     */
    public static String GetString(String[] input, String separator) {
        return GetString(new ArrayList<String>(Arrays.asList(input)), separator);
    }

    /**
     * Converts an array of strings to a single string of all items, with each item in the list being separated by a space
     * @param input The list of strings to combine together
     * @return The combined string
     */
    public static String GetString(String[] input) {
        return GetString(input, " ");
    }

    /**
     * Removes the specified characters from the end of the string (if present)
     * @param input The string to trim
     * @param trimChars The characters to trim from the input
     * @return The trimmed string
     */
    public static String TrimEnd(String input, char[] trimChars) {
        // Check if the last occurrence of the first trimChar is at the end of the input, minus the number of trimChars
        if (input.lastIndexOf(trimChars[0]) == input.length() - trimChars.length)
            return input.substring(0, input.lastIndexOf(trimChars[0]));

        // Nothing to do, return original
        return input;
    }

    /**
     * Removes the specified character from the end of the string (if present)
     * @param input The string to trim
     * @param trimChar The character to trim from the input
     * @return The trimmed string
     */
    public static String TrimEnd(String input, char trimChar) {
        return TrimEnd(input, new char[] {trimChar});
    }

    public static UUID GetUUIDFromString(String uuid) {
        try {
            return UUID.fromString(uuid);
        } catch (Exception ex) {
            return null;
        }
    }
}
