package io.gitlab.grumpybear4257.Utils;

import io.gitlab.grumpybear4257.GBUtilLib;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class BukkitUtils {
    private static FileConfiguration config = GBUtilLib.getPlugin().getConfig();

    /**
     * Sends a message to all online players with the given permission node
     * @param permission Permission node to check for
     * @param message Message to send
     */
    public static void SendToAllPlayersWithPermission(String permission, String message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasPermission(permission))
                player.sendMessage(message);
        }
    }

    /**
     * Prints either the player's display name, or their username, depending on the config value `print-displayname`
     * @param player The player to print the name for
     * @return The formatting-cleared displayname, or the username of the player
     */
    public static String PrintPlayerName(Player player) {
        String name = config.getBoolean("print-displayname") ? player.getDisplayName() : player.getName();
        return ChatColor.stripColor(name);
    }

    /**
     * Prints either the player's display name, or their username, depending on the config value `print-displayname` and if they are online
     * @param playerUUID The UUID of the player to print the name for. If null will print the name of the console sender.
     * @return The formatting cleared displayname, or the username of the player.
     */
    public static String PrintPlayerName(UUID playerUUID) {
        if (playerUUID == null)
            return Bukkit.getConsoleSender().getName();

        OfflinePlayer offlinePlayer= Bukkit.getOfflinePlayer(playerUUID);
        Player player = offlinePlayer.getPlayer();
        return player != null ?
                PrintPlayerName(player) :
                offlinePlayer.getName();
    }

    /**
     * Adds a tab complete option if the sender has the correct permission node, and there's no conflict with the current text entered
     * @param sender The sender of the tab complete event. Used for checking permission
     * @param baseCommand The command being completed. Used for checking permission
     * @param options The list of options to add to
     * @param option The option to check and add
     * @param currentText The currently entered text to create suggestions based on
     */
    public static void AddTabCompleteOption(CommandSender sender, String baseCommand, List<String> options, String option, String currentText) {
        if (sender.hasPermission("modreq." + baseCommand + "." + option) && option.startsWith(currentText))
            options.add(option);
    }

    /**
     * Adds a tab complete option if there's no conflict with the current text entered
     * @param options The list of options to add to
     * @param option The option to check and add
     * @param currentText The currently entered text to create suggestions based on
     */
    public static void AddTabCompleteOption(List<String> options, String option, String currentText) {
        if (option.startsWith(currentText))
            options.add(option);
    }

    /**
     * Informs the user of bad syntax in their command
     * @param sender The sender of the malformed command
     * @param usage The correct usage of the command
     */
    public static void BadSyntax(CommandSender sender, String usage) {
        sender.sendMessage(ChatColor.RED + "Invalid Syntax - Usage: " + ChatColor.WHITE + usage);
    }

    /**
     * Checks the sender to see if they have the passed permission node, if not, inform them they don't have permission
     * @param sender The sender to check permission of
     * @param permissionNode The permission node to check
     * @return Whether or not the player has the specified permission
     */
    public static boolean CheckPlayerPermission(CommandSender sender, String permissionNode) {
        if (!sender.hasPermission(permissionNode)) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to perform that command!");
            return false;
        } else {
            return true;
        }
    }
}
