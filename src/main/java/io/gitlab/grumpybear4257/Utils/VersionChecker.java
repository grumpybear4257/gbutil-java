package io.gitlab.grumpybear4257.Utils;

import io.gitlab.grumpybear4257.GBUtilLib;

public class VersionChecker {
    public static String GetVersion() {
        return GBUtilLib.getPlugin().getDescription().getVersion();
    }

    public static boolean UpdateAvailable() {
        //TODO implement
        // Ensure that it only checks for updates for *this* version of minecraft, not a later version.
        // Unless I do cross-version compatible jars, then I might be able to advise of a new version all the time.
        // But I should probably implement some way of checking compat with current version anyways.
        return false;
    }
}
