package io.gitlab.grumpybear4257;

import org.bukkit.plugin.java.JavaPlugin;

public class GBUtilLib extends JavaPlugin {
    private static JavaPlugin plugin;
    private static boolean pluginSet = false;

    public static void setPlugin(JavaPlugin plugin) {
        // poor mans final
        if (pluginSet)
            throw new UnsupportedOperationException("JavaPlugin is already set!");

        pluginSet = true;
        GBUtilLib.plugin = plugin;
    }

    public static JavaPlugin getPlugin() {
        return plugin;
    }
}